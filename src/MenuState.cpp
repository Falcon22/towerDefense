//
// Created by Falcon on 29.04.17.
//

#include "MenuState.h"
#include "ResourceManager/ResourceHolder.h"
#include "ResourceManager/ResourceIdentifier.h"
#include "SFML/Graphics.hpp"
#include "Button.h"
#include "Game.h"
#include "Grid.h"
#include "Constants.h"
#include "Tile.h"

MenuState::MenuState(StateManager &stack, States::Context context)
    : State(stack, context)
{


    mapSprite.setTileNumber(16);
    texture.loadFromFile("/Users/user/ClionProjects/towerDefense_m/Resources/map1.png");
    texture.setSmooth(true);
    sf::IntRect rect{ TILE_SIZE * (16 % 15), TILE_SIZE * (16 / 15), TILE_SIZE, TILE_SIZE };
    mapSprite.setTexture(texture, rect);

    sf::RenderWindow &window = *getContext().window;

    title.setFont(*getContext().font);
    title.setFillColor(sf::Color::White);
    title.setCharacterSize(80);
    title.setString("Tower Defense");
    title.setPosition({window.getSize().x / 2 - title.getLocalBounds().width / 2,
                      window.getSize().y / 10 - title.getLocalBounds().height});

    sf::Texture &panelTexture = getContext().textureHolder->get(Textures::panel);
    panelTexture.setSmooth(true);

    sf::Texture buttonTexture = getContext().textureHolder->get(Textures::button);
    buttonTexture.setSmooth(true);

    panelSprite.setTexture(panelTexture);
    //panelSprite.setPosition({static_cast<float>(window.getSize().x / 4 - panelTexture.getSize().x / 2),
     //                       static_cast<float>(window.getSize().y / 4 - panelTexture.getSize().y / 2)});
    //panelSprite.setScale({static_cast<float>(window.getSize().x / (panelTexture.getSize().x * 1.5)), static_cast<float >(window.getSize().y / (panelTexture.getSize().y * 1.5))});

    auto play = std::make_shared<gui::Button>(*getContext().soundManager);

    play->setTexture(buttonTexture);
    play->setTexture(getContext().textureHolder->get(Textures::button));
    play->setPosition({static_cast<float>(window.getSize().x / 3 - 40),
                       static_cast<float>(window.getSize().y / 2 - 40 * 3)});
    play->setScale(2.0f, 2.0f);
    play->setFont(getContext().fontHolder->get(Fonts::font1));
    play->setText("Play");
    play->setCallback([this]() {
        popState();
        pushState(States::ID::Game);
    });

    auto quit = std::make_shared<gui::Button>(*getContext().soundManager);
    quit->setTexture(buttonTexture);
    quit->setScale(2.0f, 2.0f);
    quit->setTexture(getContext().textureHolder->get(Textures::button));
    quit->setPosition({static_cast<float>(window.getSize().x / 3 - 40),
                       static_cast<float>(window.getSize().y / 2 + 49 / 4 * 3)});
    quit->setFont(getContext().fontHolder->get(Fonts::font1));
    quit->setText("Quit");
    quit->setCallback([this]() {
       popState();
    });

    container.addWidget(play);
    container.addWidget(quit);
}

bool MenuState::handleEvent(const sf::Event &event)
{
    container.handleWidgetsEvent(event);
    return false;
}

bool MenuState::update(sf::Time dt)
{
    container.updateWidgets(dt);
    return false;
}

void MenuState::draw()
{
    getContext().window->clear(sf::Color(0, 165, 80, 0));
    for (int i = 0; i < MAP_HEIGHT; i++)
        for (int j = 0; j < MAP_WIDTH; j++)
        {
            mapSprite.setPosition(static_cast<float >(j * TILE_SIZE), static_cast<float>(i * TILE_SIZE));
            getContext().window->draw(mapSprite);
        }
            //getContext().window->draw(panelSprite);
    getContext().window->draw(title);
    getContext().window->draw(container);
}