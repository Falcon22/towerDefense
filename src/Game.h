#include "StateManager.h"
#include "GameContext.h"
#include "ResourceManager/ResourceHolder.h"
#include "ResourceManager/ResourceIdentifier.h"
#include "SoundManager.h"
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

class Game
{
public:
    Game();

    void run();

private:
    void processEvents();
    void update(sf::Time frameTime);
    void draw();

    void registerStates();

    TextureHolder textureHolder;
    FontHolder fontHolder;
    SoundManager soundManager;
    sf::Font font;
    sf::Sprite cursor;
    FastForward fastForward;

    sf::RenderWindow window;
    States::Context context;
    StateManager    manager;
};
