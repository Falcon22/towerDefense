#include "Camera.h"
#include "Constants.h"
#include <iostream>
#include <cmath>

Camera::Camera(States::Context context)
        : context(context)
        , dragging(false)
{
    windowSize = context.window->getSize();
    view.setSize(static_cast<float>(windowSize.x), static_cast<float>(windowSize.y)); //480
    view.setCenter(view.getSize().x / 2, view.getSize().y / 2);

    context.window->setView(view);
}

void Camera::handleEvent(const sf::Event &event)
{
    if (event.type == sf::Event::MouseButtonPressed
        && event.key.code == sf::Mouse::Right)
    {
        initialMousePos = sf::Mouse::getPosition(*context.window);
        dragging = true;
    }
    else if (event.type == sf::Event::MouseButtonReleased
             && event.key.code == sf::Mouse::Right)
    {
        dragging = false;
    }
}

void Camera::update(sf::Time dt)
{
    if (dragging)
    {
        sf::Vector2i mousePos = sf::Mouse::getPosition(*context.window);
        sf::Vector2f worldPos = context.window->mapPixelToCoords(mousePos, view);

        int temp = initialMousePos.x - mousePos.x;

        view.move(static_cast<float>(initialMousePos.x - mousePos.x),
                  static_cast<float>(initialMousePos.y - mousePos.y));

        initialMousePos = mousePos;

        if (view.getCenter().x - view.getSize().x / 2 < 0) view.setCenter(view.getSize().x / 2, view.getCenter().y);
        else if(view.getCenter().x + view.getSize().x / 2 > MAP_WIDTH * TILE_WORLD_SIZE) view.setCenter(MAP_WIDTH * TILE_WORLD_SIZE - view.getSize().x / 2, view.getCenter().y);


        if (view.getCenter().y - view.getSize().y / 2 < 0) view.setCenter(view.getCenter().x, view.getSize().y / 2);
        else if (view.getCenter().y + view.getSize().y / 2 > MAP_HEIGHT * TILE_WORLD_SIZE) view.setCenter(view.getCenter().x, (MAP_HEIGHT * TILE_WORLD_SIZE) - view.getSize().y / 2);

        context.window->setView(view);
    }

}

void Camera::draw()
{
    float x = view.getCenter().x;
    float y = view.getCenter().y;

    x = std::floor(x);
    y = std::floor(y);

    std::cout << x << ' ' << y << std::endl;

    view.setCenter(x, y);
}

const sf::View &Camera::getView()
{
    return view;
}
