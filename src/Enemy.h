#pragma once

#include "ResourceManager/ResourceHolder.h"
#include "ResourceManager/ResourceIdentifier.h"
#include <cstddef>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/System/Time.hpp>
#include <vector>
#include <SFML/Graphics.hpp>
#include "GameContext.h"

class Enemy
{
public:
    Enemy (const Type type, const TextureHolder &texture, const sf::Vector2f &position);
    void updateEnemy (const sf::Time& time);
    void draw(sf::RenderWindow &window);

    Enemy* getPointer();
    int getCost() const;

    bool isIsLife() const;
    bool isIsDestination() const;

    void hit(const size_t damage);


    const sf::Vector2f &getPosition() const;
    void setPosition(const sf::Vector2f &position);

    const std::vector<sf::Vector2f> &getDestination() const;
    void setDestination(const std::vector<sf::Vector2f> &destination);

private:
    Type type;

    bool isLife;
    bool isMove;
    bool isDestination;

    int health;
    int cost;

    float speed;

    sf::Sprite sprite;
    int width, height;
    float currentFrame;
    float angle;

    sf::Vector2f position;
    std::vector<sf::Vector2f> destination;
    size_t currentDestinationPoint;

};

