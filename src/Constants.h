#pragma once

#define MAP_WIDTH 15
#define MAP_HEIGHT 12
#define TILE_SIZE 64
#define TILE_WORLD_SIZE 64
#define RAD_IN_DGR (180 / M_PI)
#define NUM_ENEM 30
const unsigned int MAX_COMPONENTS = 32;