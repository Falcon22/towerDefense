
#include <iostream>
#include "Enemy.h"
#include "Constants.h"
#include "GameState.h"

Enemy::Enemy(const Type type,const TextureHolder &texture, const sf::Vector2f &position)
{
    Enemy::type = type;
    isLife = true;
    isMove = true;
    isDestination = false;
    Enemy::position = position;
    currentFrame = 0;
    currentDestinationPoint = 0;
    angle = 0;
    speed = 100;
    switch (type)
    {
        case Type::lvlOne:
            health = 600;
            cost = 100;
            sprite.setTexture(texture.get(Textures::enemyOne));
            sprite.setScale({0.5, 0.5});
            width = sprite.getTextureRect().width / 4;
            height = sprite.getTextureRect().height;
            break;

        case Type::lvlTwo:
            health = 1000;
            cost = 200;
            sprite.setTexture(texture.get(Textures::enemyTwo));
            sprite.setScale({0.5, 0.5});
            width = sprite.getTextureRect().width / 4;
            height = sprite.getTextureRect().height;
            break;
        default:
            break;
    }
    sprite.setTextureRect(sf::IntRect(0, 0, width, height));
    sprite.setPosition(position);
    sprite.setOrigin(width / 2, height / 2);
}

void Enemy::updateEnemy(const sf::Time &dTime)
{
    if (currentDestinationPoint != destination.size())
    {
        if (health <= 0)
        {
            isLife = false;
            return;
        }
        if (isMove)
        {

            if (!(position.x <= destination[currentDestinationPoint].x + 10
                  && position.x >= destination[currentDestinationPoint].x - 10))
            {
                if (position.x < destination[currentDestinationPoint].x)
                {
                    position.x += speed * dTime.asSeconds();
                    angle = 0;
                }
                if (position.x > destination[currentDestinationPoint].x)
                {
                    position.x -= speed * dTime.asSeconds();
                    angle = 180;
                }
            } else {
                if ((position.y <= destination[currentDestinationPoint].y + 10
                     && position.y >= destination[currentDestinationPoint].y - 10))
                    currentDestinationPoint++;
                if (position.y < destination[currentDestinationPoint].y)
                {
                    position.y += speed * dTime.asSeconds();
                    angle = 90;
                }
                if (position.y > destination[currentDestinationPoint].y)
                {
                    position.y -= speed * dTime.asSeconds();
                    angle = 270;
                }
            }
            sprite.setRotation(angle);
            sprite.setPosition(position);
            currentFrame += 10 * dTime.asSeconds();
            if (currentFrame >= 4)
                currentFrame -= 4;
            sprite.setTextureRect(sf::IntRect(width * static_cast<int>(currentFrame), 0, width, height));

        }
    } else
    {
        isLife = false;
        isDestination = true;
    }
}

bool Enemy::isIsLife() const
{
    return isLife;
}


const sf::Vector2f &Enemy::getPosition() const
{
    return position;
}

void Enemy::setPosition(const sf::Vector2f &position)
{
    Enemy::position = position;
}

const std::vector<sf::Vector2f> &Enemy::getDestination() const
{
    return destination;
}

void Enemy::setDestination(const std::vector<sf::Vector2f> &destination)
{
    Enemy::destination = destination;
}

void Enemy::draw(sf::RenderWindow &window)
{
    window.draw(sprite);
}

Enemy* Enemy::getPointer()
{
    return this;
}

int Enemy::getCost() const
{
    return cost;
}

bool Enemy::isIsDestination() const
{
    return isDestination;
}

void Enemy::hit(const size_t damage)
{
    health -= damage;
}



