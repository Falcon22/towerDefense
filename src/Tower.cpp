
#include <SFML/System/Time.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <iostream>
#include "GameContext.h"
#include "Tower.h"
#include <cmath>

Tower::Tower(TextureHolder &textures,
             const sf::Vector2f &position, States::Context context, World::GameData &gameData)
        :attackSpeed(2),
         texture(textures),
         context(context),
         gameData(&gameData)
{
    type = Type::lvlZero;
    angle = 0;
    targetEnemy = nullptr;

    price = 1000;
    attackSpeed = 0;
    attackCooldown = 0;
    attackRange = 0;
    baseSprite.setTexture(texture.get(Textures::target));
    topSprite.setTexture(texture.get(Textures::target));
    width = topSprite.getTextureRect().width;
    height = baseSprite.getTextureRect().height;

    baseSprite.setPosition(position);
    topSprite.setTextureRect(sf::IntRect(0, 0, width, height));
    topSprite.setOrigin(width / 2, height / 2);
    topSprite.setPosition(position + sf::Vector2f(TILE_SIZE / 2, TILE_SIZE / 2));


    priceText.setFont(context.fontHolder->get(Fonts::font1));
    priceText.setFillColor(sf::Color::White);
    priceText.setCharacterSize(20);
    priceText.setString(std::to_string(price));

    circleShape.setRadius(attackRange);
    circleShape.setPosition({position.x - attackRange, position.y - attackRange});
    circleShape.setFillColor(sf::Color(0, 0, 0, 100));

    priceText.setPosition({position.x, position.y - 30});
    priceVisible = false;
    rangeVisible = false;

}

void Tower::updateTower(std::list<Enemy>& enemies, sf::Time& dTime, std::list<Bullet> &bullets) {
    if(targetEnemy) {
        if(inRange(targetEnemy->getPosition()) && targetEnemy->isIsLife()) {
            calculatingAngle(targetEnemy->getPosition());
            topSprite.setRotation(angle);
            shoot(dTime, bullets);

        } else {
            targetEnemy = nullptr;
        }
    } else {
        for (auto enemy = enemies.begin(); enemy != enemies.end(); enemy++) {
            if(inRange(enemy->getPosition()) && enemy->isIsLife()) {
                targetEnemy = enemy->getPointer();
                break;
            }
        }
    }

    if (price > gameData->gold)
        priceText.setFillColor(sf::Color::Red);
    else
        priceText.setFillColor(sf::Color::White);
}

void Tower::handleEventTower(const sf::Event &event)
{
    sf::FloatRect rect;
    rect.left = getPosition().x;
    rect.top  = getPosition().y;
    rect.width  = width;
    rect.height = height;

    switch (event.type)
    {
        case sf::Event::MouseButtonPressed:
        case sf::Event::MouseButtonReleased:
            if (rect.contains(static_cast<float>(event.mouseButton.x), static_cast<float>(event.mouseButton.y)))
            {
                if (event.type == sf::Event::MouseButtonPressed)
                {
                    if (gameData->gold >= price) {
                        gameData->gold -= price;
                        upgradeTower();
                    }


                }
                else
                {

                }
            }
            break;
        case sf::Event::MouseMoved:
            if (rect.contains(static_cast<float>(event.mouseMove.x), static_cast<float>(event.mouseMove.y)))
            {
                if (type != Type::lvlTwo)
                    priceVisible = true;
                rangeVisible = true;

            }
            else
            {
                rangeVisible = false;
                priceVisible = false;
            }
            break;
        default:
            break;
    }
}

bool Tower::inRange(const sf::Vector2f &position) const {
    return (position.x - topSprite.getPosition().x)
           * (position.x - topSprite.getPosition().x)
           + (position.y -  topSprite.getPosition().y)
             * (position.y - topSprite.getPosition().y) <= attackRange * attackRange;
}

float Tower::calculatingAngle(const sf::Vector2f &position) {
    angle = static_cast<float>(-(atan2((topSprite.getPosition().x - position.x),
                                       (topSprite.getPosition().y - position.y)) * 180 / M_PI));
    return angle;
}

void Tower::draw(sf::RenderWindow &window) {
    window.draw(baseSprite);
    window.draw(topSprite);

    if (rangeVisible)
        window.draw(circleShape);

    if (priceVisible)
        window.draw(priceText);
}

void Tower::shoot(sf::Time &dTime, std::list<Bullet> &bullets) {
    attackCooldown -= dTime.asSeconds();
    if(attackCooldown < 0) {
        attackCooldown = attackSpeed;
        Bullet bullet(bulletType, topSprite.getPosition(), context, angle, targetEnemy);
        bullets.push_back(bullet);
    }
}


sf::Vector2f Tower::getPosition() const
{
    return baseSprite.getPosition();
}


void Tower::upgradeTower()
{
    switch (type)
    {
        case Type::lvlZero:
            type = Type::lvlOne;
            bulletType = Type::lvlOne;
            baseSprite.setTexture(texture.get(Textures::towerOneBase));
            topSprite.setTexture(texture.get(Textures::towerOneTop));
            price *= 2;
            attackSpeed = 1;
            attackRange = 3 * TILE_SIZE;
            circleShape.setRadius(attackRange);
            circleShape.setPosition({topSprite.getPosition().x - attackRange, topSprite.getPosition().y - attackRange});
            attackCooldown = attackSpeed;
            priceText.setString(std::to_string(price));
            break;

        case Type::lvlOne:
            type = Type::lvlTwo;
            bulletType = Type::lvlTwo;
            baseSprite.setTexture(texture.get(Textures::towerTwoBase));
            topSprite.setTexture(texture.get(Textures::towerTwoTop));
            price = -1;
            attackRange *= 1.5;
            attackSpeed = 1;
            circleShape.setRadius(attackRange);
            circleShape.setPosition({topSprite.getPosition().x - attackRange, topSprite.getPosition().y - attackRange});
            attackCooldown = attackSpeed;
            priceText.setString("");
            break;

        default:
            break;
    }

    width = topSprite.getTextureRect().width;
    height = baseSprite.getTextureRect().height;

    topSprite.setTextureRect(sf::IntRect(0, 0, width, height));
    topSprite.setOrigin(width / 2, height / 2);
}
