#include "Bullet.h"
#include <cmath>

Bullet::Bullet(const Type type, const sf::Vector2f position, States::Context &_context, const float angle,
               Enemy* target):
        context(_context)
{
    Bullet::type = type;
    Bullet::angle = angle;
    Bullet::position = position;
    Bullet::target = target;
    isLife = true;
    isDraw = true;
    duration = 0.1;

    switch(type)
    {
        case Type::lvlOne:
            speed = 700;
            damage = 100;
            sprite.setTexture(context.textureHolder->get(Textures::bulletOne));
            width = sprite.getTextureRect().width;
            height = sprite.getTextureRect().height;

            break;

        case Type::lvlTwo:
            speed = 800;
            damage = 300;
            sprite.setTexture(context.textureHolder->get(Textures::bulletTwo));
            width = sprite.getTextureRect().width;
            height = sprite.getTextureRect().height;
            break;

        default:
            break;
    }
    sprite.setOrigin(width / 2, height / 2);
    sprite.setRotation(angle);
    sprite.setPosition(position);
}

void Bullet::updateBullet(const sf::Time &time)
{
    if (isLife && target->isIsLife())
    {
        if (!(position.x < target->getPosition().x + 20
              && position.x > target->getPosition().x - 20
              && position.y < target->getPosition().y + 20
              && position.y > target->getPosition().y - 20)) {
            angle = static_cast<float>(atan2((target->getPosition().x - position.x),
                                             (target->getPosition().y - position.y)));
            position.x += speed * time.asSeconds() * sin(angle);
            position.y += speed * time.asSeconds() * cos(angle);
            sprite.setPosition(position);
            sprite.setRotation(static_cast<float>(-angle * RAD_IN_DGR + 180));
        } else {
            sprite.setTexture(context.textureHolder->get(Textures::explosion));
            if(type == Type::lvlOne) {
                sprite.setScale(0.8, 0.8);
                sprite.setRotation(90);
            }
            isLife = false;
            target->hit(damage);
        }
    } else {
        if(!target->isIsLife() && isLife)
            duration = 0;

        duration -= time.asSeconds();

        if(duration < 0)
            isDraw = false;
    }
}

void Bullet::draw(sf::RenderWindow &window)
{
    window.draw(sprite);
}

bool Bullet::isIsDraw() const {
    return isDraw;
}

