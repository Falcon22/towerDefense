#pragma once

#include <list>
#include "Tile.h"
#include "Gui.h"

class Grid
{
public:
    explicit Grid(sf::RenderWindow &window);
    ~Grid() = default;

    void draw();

    Tile &getTile(int i, int j);

private:
    void loadFromCSV(const std::string &filename);
    void loadDecorFromCSV(const std::string &filename);

    sf::Texture texture;

    std::vector<std::vector<Tile>> matrix;
    std::vector<std::vector<Tile>> decor;
    sf::RenderWindow &window;
};


