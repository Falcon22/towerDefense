#include "GameOverState.h"

#include <fstream>
#include <cmath>

GameOverState::GameOverState(StateManager &stack, States::Context context)
    : State(stack, context)
{
    sf::RenderWindow &window = *context.window;
    sf::Font &font = *context.font;

    gameOvetText.setString("GAME OVER");
    gameOvetText.setFillColor(sf::Color::Blue);
    gameOvetText.setCharacterSize(100);
    gameOvetText.setFont(font);
    gameOvetText.setPosition({ std::floor(window.getSize().x / 2 - gameOvetText.getLocalBounds().width / 2),
                             std::floor(window.getSize().y / 2 - gameOvetText.getLocalBounds().height * 1.5f)});


    enterText.setString("ENTER Menu");
    enterText.setFillColor(sf::Color::Black);
    enterText.setCharacterSize(30);
    enterText.setFont(font);
    enterText.setPosition({ std::floor(window.getSize().x / 2 - enterText.getLocalBounds().width / 2),
                          std::floor(window.getSize().y / 2 + enterText.getLocalBounds().height * 5)});
}

bool GameOverState::handleEvent(const sf::Event &event)
{
    if (event.type == sf::Event::KeyPressed
        && event.key.code == sf::Keyboard::Return)
    {
        popState();
        pushState(States::ID::Menu);
    }

    return false;
}

bool GameOverState::update(sf::Time dt)
{
    return false;
}

void GameOverState::draw()
{
    sf::RenderWindow &window = *getContext().window;
    window.setView(window.getDefaultView());
    window.draw(gameOvetText);
    window.draw(enterText);
}