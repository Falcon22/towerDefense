//
// Created by Falcon on 29.04.17.
//

#include "Game.h"
#include "MenuState.h"
#include "GameState.h"
#include "PauseState.h"
#include "GameOverState.h"
#include "WinState.h"

#include "ResourceManager/ResourceIdentifier.h"
#include "ResourceManager/ResourceHolder.h"

Game::Game()
    : window({960, 768}, "TD", sf::Style::Titlebar | sf::Style::Close, sf::ContextSettings{0, 0, 8, 1, 1, 0, false})
    , context(window, font, textureHolder, fontHolder, soundManager, cursor, fastForward)
    , manager(context)
    , fastForward(fastForward)
{
    fastForward.setFastForward(false);
    window.setMouseCursorVisible(false);

    fontHolder.load(Fonts::font1, "/Users/user/ClionProjects/towerDefense_m/Resources/sansation.ttf");
    textureHolder.load(Textures::cursor, "/Users/user/ClionProjects/towerDefense_m/Resources/cursor.png");
    textureHolder.load(Textures::panel, "/Users/user/ClionProjects/towerDefense_m/Resources/background2.jpg");
    textureHolder.load(Textures::button, "/Users/user/ClionProjects/towerDefense_m/Resources/button.png");

    textureHolder.load(Textures::audioOn, "/Users/user/ClionProjects/towerDefense_m/Resources/audioOn.png");
    textureHolder.load(Textures::audioOff, "/Users/user/ClionProjects/towerDefense_m/Resources/audioOff.png");

    textureHolder.load(Textures::musicOn, "/Users/user/ClionProjects/towerDefense_m/Resources/musicOn.png");
    textureHolder.load(Textures::musicOff, "/Users/user/ClionProjects/towerDefense_m/Resources/musicOff.png");

    textureHolder.load(Textures::pauseOn, "/Users/user/ClionProjects/towerDefense_m/Resources/pauseOn.png");
    textureHolder.load(Textures::pauseOff, "/Users/user/ClionProjects/towerDefense_m/Resources/pauseOff.png");

    textureHolder.load(Textures::forwardOn, "/Users/user/ClionProjects/towerDefense_m/Resources/fastForwardOn.png");
    textureHolder.load(Textures::forwardOff, "/Users/user/ClionProjects/towerDefense_m/Resources/fastForwardOff.png");

    textureHolder.load(Textures::fastForwardOn, "/Users/user/ClionProjects/towerDefense_m/Resources/forwardOn.png");
    textureHolder.load(Textures::fastForwardOff, "/Users/user/ClionProjects/towerDefense_m/Resources/forwardOff.png");

    textureHolder.load(Textures::fight, "/Users/user/ClionProjects/towerDefense_m/Resources/fight.png");
    textureHolder.load(Textures::exit, "/Users/user/ClionProjects/towerDefense_m/Resources/exit.png");
    textureHolder.load(Textures::star, "/Users/user/ClionProjects/towerDefense_m/Resources/star.png");


    soundManager.loadFromFile("mouseHover", "/Users/user/ClionProjects/towerDefense_m/Resources/click4.wav");
    soundManager.loadFromFile("mouseClick", "/Users/user/ClionProjects/towerDefense_m/Resources/click4.wav");
    cursor.setTexture(textureHolder.get(Textures::cursor));
    font = fontHolder.get(Fonts::font1);

    registerStates();
    manager.pushState(States::ID::Menu);
}

void Game::run()
{
    const sf::Time frameTime = sf::seconds(1.f / 30.f);
    sf::Clock clock;
    sf::Time passedTime = sf::Time::Zero;

    while (window.isOpen())
    {
        sf::Time elapsedTime = clock.restart();

        if (fastForward.getFastForward())
            passedTime += elapsedTime + elapsedTime + elapsedTime + elapsedTime;
        else
            passedTime += elapsedTime;

        while (passedTime > frameTime)
        {
            passedTime -= frameTime;

            processEvents();
            update(frameTime);

            if (manager.isEmpty())
                window.close();
        }

        draw();
    }
}

void Game::processEvents()
{
    sf::Event event;
    while (window.pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
            window.close();

        manager.processEvents(event);
    }
}

void Game::update(sf::Time frameTime)
{
    manager.update(frameTime);

    cursor.setPosition(static_cast<sf::Vector2f>(sf::Mouse::getPosition(window)));
}

void Game::draw()
{
    window.clear(sf::Color(210, 210, 210));

    manager.draw();
    window.draw(cursor);

    window.display();
}

void Game::registerStates()
{
    manager.registerState<GameState>(States::ID::Game);
    manager.registerState<MenuState>(States::ID::Menu);
    manager.registerState<PauseState>(States::ID::Pause);
    manager.registerState<GameOverState>(States::ID::GameOver);
    manager.registerState<WinState>(States::ID::Win);
}

