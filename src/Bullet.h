#pragma once

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System/Time.hpp>
#include "ResourceManager/ResourceIdentifier.h"
#include "ResourceManager/ResourceHolder.h"
#include "Enemy.h"
#include "Tower.h"
#include "GameContext.h"

class Bullet {
public:
    Bullet(const Type type, const sf::Vector2f position, States::Context &_context, const float angle,
           Enemy* target);

    void updateBullet (const sf::Time& time);

    void draw (sf::RenderWindow &window);

    bool isIsDraw() const;

private:
    Type type;

    Enemy* target;

    size_t damage;
    size_t speed;
    sf::Vector2f position;

    sf::Sprite sprite;

    int width, height;
    float angle;

    bool isLife;
    bool isDraw;

    States::Context &context;
    float duration;

};

