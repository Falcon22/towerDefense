//
// Created by Falcon on 29.04.17.
//

#include "GameState.h"
#include <iostream>
#include "Constants.h"
#include "GameContext.h"
#include "SoundManager.h"
#include "Button.h"
#include "State.h"
#include <SFML/System/Time.hpp>
#include <fstream>
#include "Game.h"


GameState::GameState(StateManager &stack, States::Context context)
        : State(stack, context)
        , camera(context)
        , gameData()
        , hud(context, gameData)
        , map(TILE_SIZE, TILE_WORLD_SIZE)
        , grid(*context.window)
{

    fastForward = false;
    musicPlay = true;
    context.textureHolder->load(Textures::enemyOne, "Resources/enemyOne.png");
    context.textureHolder->load(Textures::enemyTwo, "Resources/enemy.png");
    context.textureHolder->load(Textures::towerOneBase, "Resources/towerOneBase.png");
    context.textureHolder->load(Textures::towerOneTop, "Resources/towerOneTop.png");

    context.textureHolder->get(Textures::towerOneBase).setSmooth(true);
    context.textureHolder->get(Textures::towerOneTop).setSmooth(true);

    context.textureHolder->load(Textures::towerTwoBase, "Resources/towerBaseTwo.png");
    context.textureHolder->load(Textures::towerTwoTop, "Resources/towerTopTwo.png");

    context.textureHolder->get(Textures::towerTwoBase).setSmooth(true);
    context.textureHolder->get(Textures::towerTwoTop).setSmooth(true);

    context.textureHolder->load(Textures::bulletOne, "Resources/bulletOne.png");
    context.textureHolder->load(Textures::bulletTwo, "Resources/bulletTwo.png");

    context.textureHolder->load(Textures::gold, "Resources/diamond.png");
    context.textureHolder->load(Textures::lives, "Resources/heart.png");
    context.textureHolder->load(Textures::target, "Resources/target.png");
    context.textureHolder->load(Textures::explosion, "Resources/explosion.png");

    music.openFromFile("/Users/user/ClionProjects/towerDefense_m/Resources/fastForward.wav");

    context.textureHolder->get(Textures::map).setSmooth(true);
    map.setTexture(context.textureHolder->get(Textures::map));

    dest.push_back(sf::Vector2f(2 * TILE_SIZE, MAP_HEIGHT * TILE_SIZE));
    dest.push_back({2 * TILE_SIZE, MAP_HEIGHT * TILE_SIZE - 4 * TILE_SIZE});
    dest.push_back({5 * TILE_SIZE, MAP_HEIGHT * TILE_SIZE - 4 * TILE_SIZE});
    dest.push_back({5 * TILE_SIZE, MAP_HEIGHT * TILE_SIZE - 7 * TILE_SIZE});
    dest.push_back({8 * TILE_SIZE, MAP_HEIGHT * TILE_SIZE - 7 * TILE_SIZE});
    dest.push_back({8 * TILE_SIZE, MAP_HEIGHT * TILE_SIZE - 2 * TILE_SIZE});
    dest.push_back({13 * TILE_SIZE, MAP_HEIGHT * TILE_SIZE - 2 * TILE_SIZE});
    dest.push_back({13 * TILE_SIZE, MAP_HEIGHT * TILE_SIZE - 10 * TILE_SIZE});
    dest.push_back({16 * TILE_SIZE, MAP_HEIGHT * TILE_SIZE - 10 * TILE_SIZE});


    waveEnemy = 20;
    distanceEnemy = 300;
    numHardEnemy = 7;
    for(int i = 0; i < waveEnemy; i++)
    {
        Enemy enemy(Type::lvlOne, *context.textureHolder, sf::Vector2f(2 * TILE_SIZE, MAP_HEIGHT * TILE_SIZE + i * distanceEnemy));
        enemy.setDestination(dest);
        enemies.push_back(enemy);
    }

    gameData.lives = 10;
    gameData.gold = 2000;
    gameData.score = 0;
    gameData.currentLevel = 0;

    Tower tower1(*context.textureHolder, sf::Vector2f(10 * TILE_SIZE, (MAP_HEIGHT - 5) * TILE_SIZE), context, gameData);
    Tower tower2(*context.textureHolder, sf::Vector2f(3 * TILE_SIZE, (MAP_HEIGHT - 6) * TILE_SIZE), context, gameData);
    Tower tower3(*context.textureHolder, sf::Vector2f(6 * TILE_SIZE, (MAP_HEIGHT - 6) * TILE_SIZE), context, gameData);
    Tower tower4(*context.textureHolder, sf::Vector2f(3 * TILE_SIZE, (MAP_HEIGHT - 3) * TILE_SIZE), context, gameData);
    Tower tower5(*context.textureHolder, sf::Vector2f(6 * TILE_SIZE, (MAP_HEIGHT - 9) * TILE_SIZE), context, gameData);
    Tower tower6(*context.textureHolder, sf::Vector2f(10 * TILE_SIZE, (MAP_HEIGHT - 7) * TILE_SIZE), context, gameData);
    Tower tower7(*context.textureHolder, sf::Vector2f(10 * TILE_SIZE, (MAP_HEIGHT - 9) * TILE_SIZE), context, gameData);
    Tower tower8(*context.textureHolder, sf::Vector2f(14 * TILE_SIZE, (MAP_HEIGHT - 9) * TILE_SIZE), context, gameData);
    Tower tower9(*context.textureHolder, sf::Vector2f(14 * TILE_SIZE, (MAP_HEIGHT - 7) * TILE_SIZE), context, gameData);
    Tower tower10(*context.textureHolder, sf::Vector2f(14 * TILE_SIZE, (MAP_HEIGHT - 5) * TILE_SIZE), context, gameData);
    Tower tower11(*context.textureHolder, sf::Vector2f(10 * TILE_SIZE, (MAP_HEIGHT - 11) * TILE_SIZE), context, gameData);
    Tower tower12(*context.textureHolder, sf::Vector2f(8 * TILE_SIZE, (MAP_HEIGHT - 9) * TILE_SIZE), context, gameData);
    Tower tower13(*context.textureHolder, sf::Vector2f(4 * TILE_SIZE, (MAP_HEIGHT - 9) * TILE_SIZE), context, gameData);
    Tower tower14(*context.textureHolder, sf::Vector2f(6 * TILE_SIZE, (MAP_HEIGHT - 4) * TILE_SIZE), context, gameData);
    Tower tower15(*context.textureHolder, sf::Vector2f(8 * TILE_SIZE, (MAP_HEIGHT - 1) * TILE_SIZE), context, gameData);
    Tower tower16(*context.textureHolder, sf::Vector2f(10 * TILE_SIZE, (MAP_HEIGHT - 1) * TILE_SIZE), context, gameData);
    Tower tower17(*context.textureHolder, sf::Vector2f(12 * TILE_SIZE, (MAP_HEIGHT - 1) * TILE_SIZE), context, gameData);

    towers.push_back(tower1);
    towers.push_back(tower2);
    towers.push_back(tower3);
    towers.push_back(tower4);
    towers.push_back(tower5);
    towers.push_back(tower6);
    towers.push_back(tower7);
    towers.push_back(tower8);
    towers.push_back(tower9);
    towers.push_back(tower10);
    towers.push_back(tower11);
    towers.push_back(tower12);
    towers.push_back(tower13);
    towers.push_back(tower14);
    towers.push_back(tower15);
    towers.push_back(tower16);
    towers.push_back(tower17);

    hud.init();

}

bool GameState::handleEvent(const sf::Event &event)
{
    camera.handleEvent(event);
    hud.handleEvent(event);
    //container.handleWidgetsEvent(event);


    for (int i = 0; i < towers.size(); i++)
        towers[i].handleEventTower(event);

    if (event.type == sf::Event::KeyPressed
        && event.key.code == sf::Keyboard::Escape)
    {
        pushState(States::ID::Pause);
    }



    if (event.type == sf::Event::MouseButtonPressed)
        if (event.mouseButton.button == sf::Mouse::Left)
        {

        }

    action = hud.getAction();

    switch (action)
    {
        case gui::HUD::Action::Forward:
            fastForward = !fastForward;

            if (fastForward && musicPlay)
                music.play();
            else
                music.stop();

            hud.setAction(gui::HUD::Action::None);
            break;

        case gui::HUD::Action::Pause :
            pushState(States::ID::Pause);
            hud.setAction(gui::HUD::Action::None);
            break;

        case gui::HUD::Action::Exit :
            popState();
            pushState(States::ID::Menu);
            hud.setAction(gui::HUD::Action::None);
            break;

        case gui::HUD::Action::Audio :
            soundPlay = !soundPlay;
            hud.setAction(gui::HUD::Action::None);
            break;

        case gui::HUD::Action::Music :
            if (musicPlay)
                music.setVolume(0.f);
            else
                music.setVolume(300.f);

            musicPlay = !musicPlay;
            hud.setAction(gui::HUD::Action::None);
            break;

        default:
            break;
    }

    getContext().fastForward->setFastForward(fastForward);

    return false;
}

bool GameState::update(sf::Time dt)
{
    sf::Time timePerFrame = sf::seconds(1.f / 30.f);

    camera.update(dt);
    hud.update(dt);
    container.updateWidgets(dt);

    sf::Vector2i pixelPos = sf::Mouse::getPosition(*getContext().window);

    if(enemies.empty())
    {
        gameData.currentLevel++;
        createWave();
    }

    for (auto enemy = enemies.begin(); enemy != enemies.end();) {
        enemy->updateEnemy(dt);
        if (!enemy->isIsLife()) {
            if (enemy->isIsDestination())
            {
                gameData.lives--;
            }
            else
            {
                gameData.gold += enemy->getCost();
                gameData.score += enemy->getCost() * 1.3;
            }

            enemy = enemies.erase(enemy);
        }
        else
            ++enemy;
    }

    for (int i = 0; i < towers.size(); i++)
    {
        towers[i].updateTower(enemies, timePerFrame, bullets);
    }

    for (auto bullet = bullets.begin(); bullet != bullets.end();) {
        bullet->updateBullet(dt);
        if (!bullet->isIsDraw())
            bullet = bullets.erase(bullet);
        else
            ++bullet;
    }

    if (gameData.lives <= 0)
    {
        popState();
        pushState(States::ID::GameOver);
    }

    /*if (gameData.lives <= 0)
    {
        popState();
        pushState(States::ID::GameOver);
    }

    if (enemies.empty())
    {
        popState();
        pushState(States::ID::Win);
    }
    */

    return false;
}

void GameState::draw()
{
    grid.draw();
    getContext().window->setView(camera.getView());


    for (auto enemy = enemies.begin(); enemy != enemies.end(); enemy++)
    {
        enemy->draw(*getContext().window);
    }

    for (auto bullet = bullets.begin(); bullet != bullets.end(); bullet++)
    {
        bullet->draw(*getContext().window);
    }

    for (int i = 0; i < towers.size(); i++)
        towers[i].draw(*getContext().window);

    getContext().window->setView(getContext().window->getDefaultView());

    getContext().window->draw(hud);
    //getContext().window->draw(container);
}

void GameState::createWave() {
    for(size_t i = 0; i < waveEnemy; i++) {
        Type  type;
        if(!(i % numHardEnemy))
            type = Type::lvlTwo;
        else
            type = Type::lvlOne;

        Enemy enemy(type, *getContext().textureHolder, sf::Vector2f(2 * TILE_SIZE, MAP_HEIGHT * TILE_SIZE + i * distanceEnemy));
        enemy.setDestination(dest);
        enemies.push_back(enemy);

    }
    if(numHardEnemy > 1)
        numHardEnemy--;

    distanceEnemy /= 1.5;
}