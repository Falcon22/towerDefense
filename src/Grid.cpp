#include <fstream>
#include <iostream>
#include "Grid.h"
#include "Constants.h"

Grid::Grid(sf::RenderWindow &window)
        : window(window)
{
    texture.loadFromFile("/Users/user/ClionProjects/towerDefense_m/Resources/map1.png");
    texture.setSmooth(true);

    matrix.resize(MAP_HEIGHT, std::vector<Tile>(MAP_WIDTH));
    decor.resize(MAP_HEIGHT, std::vector<Tile>(MAP_WIDTH));

    loadFromCSV("/Users/user/ClionProjects/towerDefense_m/Resources/map.csv");
    loadDecorFromCSV("/Users/user/ClionProjects/towerDefense_m/Resources/decoration.csv");
}

void Grid::draw()
{
    for (int i = 0; i < MAP_HEIGHT; i++)
        for (int j = 0; j < MAP_WIDTH; j++) {
            window.draw(matrix[i][j]);
            window.draw(decor[i][j]);
        }
}

Tile &Grid::getTile(int i, int j)
{
    return matrix[i][j];
}

void Grid::loadFromCSV(const std::string &filename)
{
    std::ifstream fin(filename);

    char comma;
    int tileNumber;

    for (int i = 0; i < MAP_HEIGHT; i++)
    {
        for (int j = 0; j < MAP_WIDTH; j++)
        {
            fin >> tileNumber;

            matrix[i][j].setTileNumber(tileNumber);
            sf::IntRect rect{ TILE_SIZE * (tileNumber % 15), TILE_SIZE * (tileNumber / 15), TILE_SIZE, TILE_SIZE };
            matrix[i][j].setTexture(texture, rect);
            matrix[i][j].setPosition(static_cast<float>(j * TILE_SIZE), static_cast<float>(i * TILE_SIZE));




            fin >> comma;
        }
    }

    fin.close();
}

void Grid::loadDecorFromCSV(const std::string &filename)
{
    std::ifstream fin(filename);

    char comma;
    int tileNumber;

    for (int i = 0; i < MAP_HEIGHT; i++)
    {
        for (int j = 0; j < MAP_WIDTH; j++)
        {
            fin >> tileNumber;

            decor[i][j].setTileNumber(tileNumber);
            if (tileNumber != 0)
            {
                sf::IntRect rect{TILE_SIZE * (tileNumber % 9 + 15), TILE_SIZE * 5, TILE_SIZE, TILE_SIZE};
                decor[i][j].setTexture(texture, rect);
                decor[i][j].setPosition(static_cast<float>(j * TILE_SIZE), static_cast<float>(i * TILE_SIZE));
            }




            fin >> comma;
        }
    }

    fin.close();
}